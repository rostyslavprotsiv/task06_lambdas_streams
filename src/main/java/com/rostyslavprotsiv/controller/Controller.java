package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.action.*;
import com.rostyslavprotsiv.model.action.command.ObjectCommand;
import com.rostyslavprotsiv.model.creator.StreamCreator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public final class Controller {
    private final Logger LOGGER = LogManager.getLogger(Controller.class);
    private final FuncLambdaAction FUNC_LAMBDA_ACTION = new FuncLambdaAction();
    private final Receiver RECEIVER = new Receiver();
    private final ObjectCommand OBJECT_COMMAND = new ObjectCommand(RECEIVER);
    private final CommandAction COMMAND_ACTION = new CommandAction(RECEIVER);
    private final Invoker INVOKER = new Invoker();
    private final StreamAction STREAM_ACTION= new StreamAction();
    private final List<Integer> GENERATED = new StreamCreator().getRandomList();
    private final WordAction WORD_ACTION = new WordAction();


    public int getMaxValueUsingLambda(int a, int b, int c) {
        return FUNC_LAMBDA_ACTION.getMaxUsingLambda(a, b, c);
    }

    public int getAverageUsingLambda(int a, int b, int c) {
        return FUNC_LAMBDA_ACTION.getAverageUsingLambda(a, b, c);
    }

    public String getLambdaCommand(String arg) {
        INVOKER.addCommand(COMMAND_ACTION.lambdaCommand());
        return INVOKER.run(arg);
    }

    public String getObjectCommand(String arg) {
        INVOKER.addCommand(OBJECT_COMMAND);
        return INVOKER.run(arg);
    }

    public String getAnonymousCommand(String arg) {
        INVOKER.addCommand(COMMAND_ACTION.anonymousCommand());
        return INVOKER.run(arg);
    }

    public String getMethodReferenceCommand(String arg) {
        INVOKER.addCommand(COMMAND_ACTION.methodReferenceCommand());
        return INVOKER.run(arg);
    }

    public List<Integer> getGeneratedList() {
        if(GENERATED.size() == 0) {
            String logMessage = "Used incorrect creator ";
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
            return null;
        }
        return GENERATED;
    }

    public double findAverageUsingStream() {
        return STREAM_ACTION.findAverageUsingStream(GENERATED);
    }

    public int findMinUsingStream() {
        return STREAM_ACTION.findMinUsingStream(GENERATED);
    }

    public int findMaxUsingStream() {
        return STREAM_ACTION.findMaxUsingStream(GENERATED);
    }

    public int findSumUsingSum() {
        return STREAM_ACTION.findSumUsingSum(GENERATED);
    }

    public int findSumUsingReduce() {
        return STREAM_ACTION.findSumUsingReduce(GENERATED);
    }

    public long countBiggerThatAverage() {
        return STREAM_ACTION.countBiggerThatAverage(GENERATED);
    }

    public long getUniqueWords(Stream<String> stream) {
        return WORD_ACTION.uniqueWords(stream);
    }

    public List<String> getSortedUniqueWords(Stream<String> stream) {
        return WORD_ACTION.sortedUniqueWords(stream);
    }

    public Map<String, Long> getCountWords(Stream<String> stream) {
        return WORD_ACTION.countWords(stream);
    }

    public Map<Integer, String> getOccurrenceNumber(Stream<String> stream) {
        return WORD_ACTION.occurrenceNumberExceptUpper(stream);
    }
}
