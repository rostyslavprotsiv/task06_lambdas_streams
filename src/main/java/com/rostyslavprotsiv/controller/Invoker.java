package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.action.command.Command;

public class Invoker {
    private Command command;

    public void addCommand(Command command) {
        this.command = command;
    }

    public String run(String arg) {
        return command.execute(arg);
    }
}
