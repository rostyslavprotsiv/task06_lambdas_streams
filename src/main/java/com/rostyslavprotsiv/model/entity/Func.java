package com.rostyslavprotsiv.model.entity;

@FunctionalInterface
public interface Func {
    int act(int first, int second, int third);
}
