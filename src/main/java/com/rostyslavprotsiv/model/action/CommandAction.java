package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.action.command.Command;

public class CommandAction {
    private Receiver receiver;

    public CommandAction(Receiver receiver) {
        this.receiver = receiver;
    }

    public Command lambdaCommand() {
        return (str) -> receiver.outCommandWithLambda(str);
    }

    public Command anonymousCommand() {
        return new Command() {
            @Override
            public String execute(String str) {
                return receiver.outCommandWithAnonymousClass(str);
            }
        };
    }

    public Command methodReferenceCommand() {
        return (receiver::outCommandWithMethodReference);
    }
}
