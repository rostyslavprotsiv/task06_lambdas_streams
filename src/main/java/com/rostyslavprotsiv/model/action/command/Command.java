package com.rostyslavprotsiv.model.action.command;

@FunctionalInterface
public interface Command {
    String execute(String str);
}
