package com.rostyslavprotsiv.model.action.command;

import com.rostyslavprotsiv.model.action.Receiver;

public class ObjectCommand implements Command {
    private Receiver receiver;

    public ObjectCommand(Receiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public String execute(String str) {
        return receiver.outCommandWithSimpleClass(str);
    }

}
