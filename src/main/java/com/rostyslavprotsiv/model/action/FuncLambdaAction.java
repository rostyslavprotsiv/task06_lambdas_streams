package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.Func;

public class FuncLambdaAction {
    public int getMaxUsingLambda(int a, int b, int c) {
        Func first = (a1, b1, c1) -> {
            if (a1 > b1 && a1 > c1) {
                return a1;
            } else if (b1 > a1 && b1 > c1) {
                return b1;
            } else {
                return c1;
            }
        };
        return first.act(a, b, c);
    }

    public int getAverageUsingLambda(int a, int b, int c) {
        Func second = (a1, b1, c1) -> (a1 + b1 + c1) / 3;
        return second.act(a, b, c);
    }
}
