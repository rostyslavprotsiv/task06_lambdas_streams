package com.rostyslavprotsiv.model.action;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.OptionalDouble;
import java.util.OptionalInt;

public class StreamAction {
    private final Logger LOGGER = LogManager.getLogger(StreamAction.class);
    public double findAverageUsingStream(List<Integer> list) {
        OptionalDouble average = list.stream().mapToInt(e -> e).average();
        if (average.isPresent()) {
            return average.getAsDouble();
        }
        return -1;
    }

    public int findMinUsingStream(List<Integer> list) {
        OptionalInt min = list.stream().mapToInt(e -> e).min();
        if (min.isPresent()) {
            return min.getAsInt();
        }
        return -1;
    }

    public int findMaxUsingStream(List<Integer> list) {
        OptionalInt max = list.stream().mapToInt(e -> e).max();
        if (max.isPresent()) {
            return max.getAsInt();
        }
        return -1;
    }

    public int findSumUsingSum(List<Integer> list) {
        int sum = list.stream().mapToInt(e -> e).sum();
        return sum;
    }

    public int findSumUsingReduce(List<Integer> list) {
        OptionalInt sum = list.stream().mapToInt(e -> e).reduce((a, b) -> a + b);
        if (sum.isPresent()) {
            return sum.getAsInt();
        }
        String logMessage = "Used incorrect argument ";
        LOGGER.info(logMessage);
        LOGGER.warn(logMessage);
        LOGGER.error(logMessage);
        return -1;
    }

    public long countBiggerThatAverage(List<Integer> list) {
        long sum = list.stream().mapToInt(e -> e).filter(a -> a > findAverageUsingStream(list)).count();
        return sum;
    }
}
