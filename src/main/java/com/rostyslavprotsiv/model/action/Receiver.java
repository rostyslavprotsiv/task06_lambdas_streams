package com.rostyslavprotsiv.model.action;


public class Receiver {

    public String outCommandWithLambda(String str) {
        return "Information from command that is implemented by lambda :" + str;
    }

    public String outCommandWithAnonymousClass(String str) {
        return "Information from command that is implemented by Anonymous class : " + str;
    }

    public String outCommandWithSimpleClass(String str) {
        return "Information from command that is implemented by simple class : " + str;
    }

    public String outCommandWithMethodReference(String str) {
        return "Information from command that is implemented by method reference : " + str;
    }
}
