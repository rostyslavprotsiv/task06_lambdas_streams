package com.rostyslavprotsiv.model.action;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WordAction {

    public long uniqueWords(Stream<String> stream) {
        return stream.distinct()
                .count();
    }

    public List<String> sortedUniqueWords(Stream<String> stream) {
        return stream
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    public Map<String, Long> countWords(Stream<String> stream) {
        return stream
                .collect(Collectors.groupingBy(String::toString, Collectors.counting()));
    }

    public Map<Integer, String> occurrenceNumberExceptUpper(Stream<String> stream) {
        List<String> list = stream
                .flatMap(e -> Stream.of(e.split("")))
                .filter(s -> s != s.toUpperCase())
                .collect(Collectors.toList());
        Map<Integer, String> map = Stream
                .iterate(1, s -> s + 1)
                .limit(list.size())
                .collect(Collectors.toMap(i -> i, i -> list.get(i - 1)));
        return map;
    }
}
