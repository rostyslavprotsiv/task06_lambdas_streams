package com.rostyslavprotsiv.model.creator;

import java.util.*;

public class StreamCreator {
    private final Random RANDOM = new Random();
    private final int MAX_VALUE_OF_MASS = 15;
    private final int MAX_VALUE_OF_ELEMENT = 35;
    private final int MIN_VALUE_OF_ELEMENT = -24;

    public List<Integer> getRandomList() {
        final int size = RANDOM.nextInt(MAX_VALUE_OF_MASS) + 1;
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            list.add(RANDOM.nextInt(MAX_VALUE_OF_ELEMENT - MIN_VALUE_OF_ELEMENT) + MIN_VALUE_OF_ELEMENT);
        }
        return list;
    }

    public int[] getRandomArray() {
        final int size = RANDOM.nextInt(MAX_VALUE_OF_MASS) + 1;
        int[] array = new int[size];
        for (int i = 0; i < size; i++) {
            array[i] = RANDOM.nextInt(MAX_VALUE_OF_ELEMENT - MIN_VALUE_OF_ELEMENT) + MIN_VALUE_OF_ELEMENT;
        }
        return array;
    }
}
