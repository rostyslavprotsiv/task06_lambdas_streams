package com.rostyslavprotsiv.view;

public final class Menu {
    private  final MenuUsingLambda MENU_USING_LAMBDA = new MenuUsingLambda();
    private  final Client CLIENT= new Client();
    private  final MenuUsingStream MENU_USING_STREAM = new MenuUsingStream();
    private  final MenuWordsWork MENU_WORDS_WORK = new MenuWordsWork();

    public void show() {
        MENU_USING_LAMBDA.show();
        CLIENT.show();
        MENU_USING_STREAM.show();
        MENU_WORDS_WORK.show();
    }

}
