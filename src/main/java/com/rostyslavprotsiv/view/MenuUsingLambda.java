package com.rostyslavprotsiv.view;

import java.util.Arrays;

public class MenuUsingLambda extends AbstractMenu {

    MenuUsingLambda() {
        menu.put(0, "Max value");
        menu.put(1, "Average");
        menu.put(2, "Quit");
        menuForMethods.put(0, this::printMax);
        menuForMethods.put(1, this::printAverage);
        menuForMethods.put(2, this::quit);
    }

    private int[] input() {
        int[] in = new int[3];
        LOGGER.info("Input three numbers : ");
        for (int i = 0; i < in.length; i++) {
            if (scan.hasNextInt()) {
                in[i] = scan.nextInt();
            } else {
                return null;
            }
        }
        return in;
    }

    private void printMax() {
        int[] inputted = input();
        if (inputted == null) {
            badInput();
            String logMessage = "Inputted incorrect values ";
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
            LOGGER.fatal(logMessage);
            return;
        }
        LOGGER.info("Max value : " + CONTROLLER.getMaxValueUsingLambda(inputted[0],
                inputted[1], inputted[2]));
    }

    private void printAverage() {
        int[] inputted = input();
        if (inputted == null) {
            badInput();
            return;
        }
        LOGGER.info("Average : " + CONTROLLER.getAverageUsingLambda(inputted[0],
                inputted[1], inputted[2]));
    }

    @Override
    protected void showInfo() {
        LOGGER.info("MenuUsingLambda");
        menu.forEach((key, elem) -> LOGGER.info(key + " : " + elem));
    }
}

