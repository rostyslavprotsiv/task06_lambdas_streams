package com.rostyslavprotsiv.view;

public class MenuUsingStream extends AbstractMenu{

    MenuUsingStream() {
        menu.put(0,CONTROLLER.getGeneratedList().toString());
        menu.put(1, "Average");
        menu.put(2, "Min");
        menu.put(3, "Max");
        menu.put(4, "Sum using sum()");
        menu.put(5, "Sum using reduce()");
        menu.put(6, "Bigger than average");
        menu.put(7, "Quit");
        menuForMethods.put(1, this::printAverage);
        menuForMethods.put(2, this::printMin);
        menuForMethods.put(3, this::printMax);
        menuForMethods.put(4, this::printSumUsingSum);
        menuForMethods.put(5, this::printSumUsingReduce);
        menuForMethods.put(6, this::printCountBiggerThanAverage);
        menuForMethods.put(7, this::quit);
    }

    private void printAverage() {
        LOGGER.info(CONTROLLER.findAverageUsingStream());
    }

    private void printMin() {
        LOGGER.info(CONTROLLER.findMinUsingStream());
    }

    private void printMax() {
        LOGGER.info(CONTROLLER.findMaxUsingStream());
    }

    private void printSumUsingSum() {
        LOGGER.info(CONTROLLER.findSumUsingSum());
    }

    private void printSumUsingReduce() {
        LOGGER.info(CONTROLLER.findSumUsingReduce());
    }

    private void printCountBiggerThanAverage() {
        LOGGER.info(CONTROLLER.countBiggerThatAverage());
    }

    @Override
    protected void showInfo() {
        LOGGER.info("Menu using streams");
        menu.forEach((key, elem) -> LOGGER.info(key + " : " + elem));
    }

}
