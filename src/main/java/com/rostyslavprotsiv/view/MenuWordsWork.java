package com.rostyslavprotsiv.view;

import com.rostyslavprotsiv.controller.Controller;
import com.rostyslavprotsiv.view.interfaces.PrintableForWordsWork;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Stream;

public class MenuWordsWork {
    private Map<Integer, String> menu = new HashMap<>();
    private Map<Integer, PrintableForWordsWork> menuForMethods = new HashMap<>();
    private final Controller CONTROLLER = new Controller();
    private Scanner scan = new Scanner(System.in);
    private final Logger LOGGER = LogManager.getLogger(MenuWordsWork.class);

    MenuWordsWork() {
        menu.put(0, "Number of unique words");
        menu.put(1, "Sorted unique words");
        menu.put(2, "Word count");
        menu.put(3, "Occurrence number of each symbol " +
                "except upper case characters");
        menu.put(4, "Quit");
        menuForMethods.put(0, this::printNumberOfUniqueWords);
        menuForMethods.put(1, this::printSortedUniqueWords);
        menuForMethods.put(2, this::printCountWords);
        menuForMethods.put(3, this::printOccurrenceNumber);
    }

    public void show() {
        Stream.Builder<String> streamBuilder = Stream.builder();
        LOGGER.info("Please, input needed text");
        String inputted = scan.nextLine();
        do {
            streamBuilder.add(inputted);
            inputted = scan.nextLine();
        } while (!inputted.equals(""));
        choosePointOfMenu(streamBuilder.build());
    }

    private void choosePointOfMenu(Stream<String> stream) {
        int inp;
        do {
            showMenu();
            scan = new Scanner(System.in);
            if (scan.hasNextInt() && (inp = scan.nextInt())
                    < menu.size() && inp >= 0) {
                if (inp == menu.size() - 1) {
                    break;
                }
                menuForMethods.get(inp).print(stream);
            } else {
                LOGGER.error("Inputted bad value");
                continue;
            }
            break;
        } while (true);
        LOGGER.info("Bye Bye");
    }

    private void showMenu() {
        LOGGER.info("Menu for Task 4");
        menu.forEach((key, elem) -> LOGGER.info(key + " : " + elem));
    }

    private void printNumberOfUniqueWords(Stream<String> stream) {
        LOGGER.info(CONTROLLER.getUniqueWords(stream));
    }

    private void printSortedUniqueWords(Stream<String> stream) {
        LOGGER.info(CONTROLLER.getSortedUniqueWords(stream));
    }

    private void printCountWords(Stream<String> stream) {
        LOGGER.info(CONTROLLER.getCountWords(stream));
    }

    private void printOccurrenceNumber(Stream<String> stream) {
        LOGGER.info(CONTROLLER.getOccurrenceNumber(stream));
    }
}
