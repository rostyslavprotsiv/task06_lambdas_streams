package com.rostyslavprotsiv.view.interfaces;

import java.util.stream.Stream;

@FunctionalInterface
public interface PrintableForWordsWork {
    void print(Stream<String> stream);
}
