package com.rostyslavprotsiv.view.interfaces;

@FunctionalInterface
public interface PrintableForClient {
    void print(String str);
}
