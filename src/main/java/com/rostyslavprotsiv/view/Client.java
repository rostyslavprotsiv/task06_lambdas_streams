package com.rostyslavprotsiv.view;

import com.rostyslavprotsiv.controller.Controller;
import com.rostyslavprotsiv.view.interfaces.PrintableForClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Client {
    private Map<Integer, String> menu = new HashMap<>();
    private Map<Integer, PrintableForClient> menuForMethods = new HashMap<>();
    private final Controller CONTROLLER = new Controller();
    private Scanner scan = new Scanner(System.in);
    private final Logger LOGGER = LogManager.getLogger(Client.class);

    Client() {
        menu.put(0, "Lambda expression");
        menu.put(1, "Simple class");
        menu.put(2, "Anonymous class");
        menu.put(3, "Method reference");
        menu.put(4, "Quit");
        menuForMethods.put(0, this::printLambda);
        menuForMethods.put(1, this::printObject);
        menuForMethods.put(2, this::printAnonymous);
        menuForMethods.put(3, this::printMethodReference);
        menuForMethods.put(4, this::exit);
    }

    void show() {
        int inputted;
        do {
            showInfo();
            scan = new Scanner(System.in);
            if (scan.hasNextInt() && (inputted = scan.nextInt())
                    < menu.size() && inputted >= 0) {
                if (inputted == menu.size() - 1) {
                    break;
                }
                LOGGER.info("Please, input string");
                menuForMethods.get(inputted).print(scan.next());
            } else {
                exit("\u001B[31m" + "You entered bad value" + "\u001B[0m");
            }
        } while (true);
        exit("Bye Bye");
    }

    private void showInfo() {
        LOGGER.info("Menu using Command pattern");
        menu.forEach((key, elem) -> LOGGER.info(key + " : " + elem));
    }

    private void printLambda(String str) {
        LOGGER.info(CONTROLLER.getLambdaCommand(str));
    }

    private void printObject(String str) {
        LOGGER.info(CONTROLLER.getObjectCommand(str));
    }

    private void printAnonymous(String str) {
        LOGGER.info(CONTROLLER.getAnonymousCommand(str));
    }

    private void printMethodReference(String str) {
        LOGGER.info(CONTROLLER.getMethodReferenceCommand(str));
    }

    private void exit(String str) {
        LOGGER.info(str);
    }
}
